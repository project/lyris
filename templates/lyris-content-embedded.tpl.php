<?php
/**
 * @file
 * Return markup for an indicator containing the list name and content name if
 * applicable with links to navigate to the edit forms.
 */
?>
<div id="lyris-content-embedded">
  <iframe src="<?php print $content_path; ?>" name="lyris-content-frame" id="lyris-content-frame" frameborder="0">
    Content goes here.
  </iframe>
</div>
